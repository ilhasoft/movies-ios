//
//  ISFMovie.swift
//  IlhasoftFilmes
//
//  Created by Daniel Amaral on 19/06/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class ISFMovie: Object, Mappable {

    dynamic var imdbID:String!
    dynamic var title:String!
    dynamic var year:String!
    dynamic var runTime:String!
    dynamic var genre:String!
    dynamic var plot:String!
    dynamic var poster:String?
    dynamic var imdbRating:String?
    
    override static func primaryKey() -> String? {
        return "imdbID"
    }
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.imdbID       <- map["imdbID"]
        self.title        <- map["Title"]
        self.year         <- map["Year"]
        self.runTime      <- map["Runtime"]
        self.genre        <- map["Genre"]
        self.plot         <- map["Plot"]
        self.poster       <- map["Poster"]
        self.imdbRating   <- map["imdbRating"]
    }
    
}
