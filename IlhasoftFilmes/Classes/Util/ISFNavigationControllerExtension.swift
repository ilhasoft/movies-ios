//
//  ISFNavigationControllerExtension.swift
//  IlhasoftFilmes
//
//  Created by Daniel Amaral on 20/06/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit

extension UINavigationController {
    public override func shouldAutorotate() -> Bool {
        return !(self.topViewController is ISFMovieDetailViewController)
    }
}
