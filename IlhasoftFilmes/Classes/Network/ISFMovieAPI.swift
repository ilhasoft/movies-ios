//
//  ISFMovieAPI.swift
//  IlhasoftFilmes
//
//  Created by Daniel Amaral on 19/06/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ISFMovieAPI: NSObject {

    static let baseURL = "http://www.omdbapi.com/"
    
    class func searchMovieByName(name:String,completion:(movies:[ISFMovie]?,error:String?) -> Void) {
    
        let url = "\(baseURL)?s=\(name.stringByReplacingOccurrencesOfString(" ", withString: "%20"))&y=&plot=short&r=json"
        
        Alamofire.request(.GET, url, parameters: nil, encoding: .JSON).responseArray(keyPath: "Search") { (response: Response<[ISFMovie], NSError>) in
            if let _ = response.result.error {
                completion(movies:nil,error: "Vídeo não encontrado")
            }else {
                completion(movies: response.result.value!, error: nil)
            }
        }
        
    }
    
    class func getMovieByID(imdbID:String,completion:(movie:ISFMovie) -> Void) {
        
        let url = "\(baseURL)?i=\(imdbID)&plot=short&r=json"
        
        Alamofire.request(.GET, url, parameters: nil, encoding: .JSON).responseObject { (response: Response<ISFMovie,NSError>) in
            if let error = response.result.error {
                print(error.localizedDescription)
            }else {
                completion(movie: response.result.value!)
            }
        }
        
    }
    
}
