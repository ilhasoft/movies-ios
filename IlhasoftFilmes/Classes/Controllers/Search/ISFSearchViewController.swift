//
//  ISFSearchViewController.swift
//  IlhasoftFilmes
//
//  Created by Daniel Amaral on 19/06/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit
import ProgressHUD

protocol ISFSearchViewControllerDelegate {
    func movieWasChosen(movie:ISFMovie)
}

class ISFSearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet var searchBar:UISearchBar!
    @IBOutlet var tableView:UITableView!
    
    var movieList = [ISFMovie]()
    
    var delegate:ISFSearchViewControllerDelegate?
    
    init() {
        super.init(nibName: "ISFSearchViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.searchBar.becomeFirstResponder()
        self.movieList = []
        self.searchBar.text = ""
        self.tableView.reloadData()
    }
    
    //MARK: TableView
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(NSStringFromClass(ISFSearchTableViewCell.self), forIndexPath: indexPath) as! ISFSearchTableViewCell
        
        let movie = movieList[indexPath.row]
        cell.setupData(movie)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let movie = movieList[indexPath.row]
        delegate?.movieWasChosen(movie)
        self.navigationController?.popViewControllerAnimated(true)
    }

    //MARK: Class Methods
    
    func setupTableView() {
        self.tableView.separatorColor = UIColor.clearColor()
        self.tableView.registerNib(UINib(nibName: "ISFSearchTableViewCell", bundle: nil), forCellReuseIdentifier: NSStringFromClass(ISFSearchTableViewCell.self))
    }
    
    //MARK: UISearchBar
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        
        if searchBar.text?.characters.count > 0 {
            ProgressHUD.show(nil)
            
            ISFMovieAPI.searchMovieByName(searchBar.text!, completion: { (movies,error) in
                ProgressHUD.dismiss()
                
                if let movies = movies {
                    searchBar.resignFirstResponder()
                    self.movieList = movies
                }else if let error = error {
                    self.movieList = []
                    UIAlertView(title: nil, message: error, delegate: self, cancelButtonTitle: "OK").show()
                }
                
                self.tableView.reloadData()
            })
        }
    }
    
}
