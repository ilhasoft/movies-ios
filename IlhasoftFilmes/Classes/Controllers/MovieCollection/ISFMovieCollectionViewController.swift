//
//  ISFMovieCollectionViewController.swift
//  IlhasoftFilmes
//
//  Created by Daniel Amaral on 19/06/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit
import RealmSwift
import ProgressHUD

class ISFMovieCollectionViewController: UICollectionViewController, ISFSearchViewControllerDelegate, UICollectionViewDelegateFlowLayout {

    var movieList:Results<ISFMovie>!
    let realm = try! Realm()
    
    let searchViewController = ISFSearchViewController()
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nil, bundle: nil)
    }
    
    init() {
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.minimumLineSpacing = 1.0
        collectionViewFlowLayout.minimumInteritemSpacing = 1.0
        collectionViewFlowLayout.scrollDirection = UICollectionViewScrollDirection.Vertical;
        super.init(collectionViewLayout: collectionViewFlowLayout)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Ilhasoft Movies"
        
        loadMovies()
        searchViewController.delegate = self
        setupCollectionView()
        setupRightButtomItem()
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieList.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(NSStringFromClass(ISFMovieCollectionViewCell.self), forIndexPath: indexPath) as! ISFMovieCollectionViewCell
    
        cell.setupData(self.movieList[indexPath.item])
    
        return cell
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let movie = (collectionView.cellForItemAtIndexPath(indexPath) as! ISFMovieCollectionViewCell).movie
        
        self.navigationController?.pushViewController(ISFMovieDetailViewController(movie: movie), animated: true)
        
    }
    
    //MARK: ISFSearchViewControllerDelegate
    
    func movieWasChosen(movie: ISFMovie) {
        ProgressHUD.show(nil)
        ISFMovieAPI.getMovieByID(movie.imdbID, completion: { (movie) in
            ProgressHUD.dismiss()
            
            try! self.realm.write {
                self.realm.add(movie,update: true)
            }
            
            self.collectionView?.reloadData()
            
        })
    }
    
    //MARK: Class Methods
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        self.collectionView?.performBatchUpdates(nil, completion: nil)
    }
    
    func loadMovies() {
        self.movieList = realm.objects(ISFMovie)
        
        if movieList.isEmpty {
            let alertController = UIAlertController(title: nil, message: "Você ainda não cadastrou nenhum filme. Cadastre o Primeiro!", preferredStyle: UIAlertControllerStyle.Alert)
            
            alertController.addAction(UIAlertAction(title: "Cadastrar", style: UIAlertActionStyle.Default, handler: { (action) in
                self.openSearchController()
            }))
            
            alertController.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.Cancel, handler: { (action) in }))
            
            self.presentViewController(alertController, animated: true, completion: nil)
            
        }else {
            self.collectionView?.reloadData()
        }
        
        
    }
    
    func getCellItemSize() -> CGSize {
        let numberOfColumns:CGFloat = 3
        let itemWidth = (CGRectGetWidth(self.collectionView!.frame) - (numberOfColumns - 1)) / numberOfColumns
        return CGSizeMake(itemWidth, 200)

    }
    
    func setupRightButtomItem() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: #selector(openSearchController))
    }
    
    func setupCollectionView() {
        self.collectionView!.registerNib(UINib(nibName: "ISFMovieCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: NSStringFromClass(ISFMovieCollectionViewCell.self))
    }
    
    func openSearchController() {
        self.navigationController?.pushViewController(searchViewController, animated: true)
    }
    
    //MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return getCellItemSize()
    }


}
