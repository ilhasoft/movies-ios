//
//  ISFMovieDetailViewController.swift
//  IlhasoftFilmes
//
//  Created by Daniel Amaral on 20/06/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit

class ISFMovieDetailViewController: UIViewController {

    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var lbRate: UILabel!
    @IBOutlet weak var viewRate: UIView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbOtherInfos: UILabel!
    @IBOutlet weak var lbPlot: UILabel!
    
    var movie:ISFMovie!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupData()
    }

    init(movie:ISFMovie) {
        self.movie = movie
        super.init(nibName: "ISFMovieDetailViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Class Methods
    
    func setupUI() {
        self.viewRate.layer.cornerRadius = 5
    }
    
    func setupData() {
        
        self.lbName.text = movie.title
        self.lbOtherInfos.text = "\(movie.year) - \(movie.runTime) - \(movie.genre)"
        self.lbPlot.text = movie.plot
        self.lbRate.text = movie.imdbRating
        
        if let poster = movie.poster where poster != ISFImageContent.None.rawValue {
            self.imgPoster.sd_setImageWithURL(NSURL(string: poster))
        }else {
            self.imgPoster.image = UIImage(named: "default")
        }
        
    }
    
    
}
