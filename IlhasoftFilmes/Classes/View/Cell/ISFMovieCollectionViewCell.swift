//
//  ISFMovieCollectionViewCell.swift
//  IlhasoftFilmes
//
//  Created by Daniel Amaral on 19/06/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit
import SDWebImage

enum ISFImageContent:String {
    case None = "N/A"
}

class ISFMovieCollectionViewCell: UICollectionViewCell {

    @IBOutlet var lbName:UILabel!
    @IBOutlet var lbYear:UILabel!
    @IBOutlet var imgPoster:UIImageView!
    
    var movie:ISFMovie!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    //MARK: Class Methods
    
    func setupData(movie:ISFMovie) {
        
        self.movie = movie
        self.lbName.text = movie.title
        self.lbYear.text = movie.year
        
        if let poster = movie.poster where poster != ISFImageContent.None.rawValue {
            self.imgPoster.sd_setImageWithURL(NSURL(string: poster))
        }else {
            self.imgPoster.image = UIImage(named: "default")
        }
    }
    
}
