//
//  ISFSearchTableViewCell.swift
//  IlhasoftFilmes
//
//  Created by Daniel Amaral on 20/06/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit

class ISFSearchTableViewCell: UITableViewCell {

    @IBOutlet var lbName:UILabel!
    
    var movie:ISFMovie!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        super.selectionStyle = UITableViewCellSelectionStyle.None
    }
    
    //MARK: Class Methods
    
    func setupData(movie:ISFMovie) {
        self.movie = movie
        self.lbName.text = movie.title
    }
    
}
