//
//  AppDelegate.swift
//  IlhasoftFilmes
//
//  Created by Daniel Amaral on 19/06/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        self.window?.backgroundColor = UIColor.blackColor()
        self.window?.rootViewController = setupNavigation(ISFMovieCollectionViewController())
        self.window?.makeKeyAndVisible()
        
        return true
    }
    
    func setupNavigation(rootViewController:UIViewController) -> UINavigationController {
        let navigationController = UINavigationController(rootViewController: ISFMovieCollectionViewController())
        navigationController.navigationBar.barStyle = UIBarStyle.BlackTranslucent
        navigationController.navigationBar.tintColor = UIColor.whiteColor()
        return navigationController
    }

    func applicationWillResignActive(application: UIApplication) {
    }

    func applicationDidEnterBackground(application: UIApplication) {
    }

    func applicationWillEnterForeground(application: UIApplication) {
    }

    func applicationDidBecomeActive(application: UIApplication) {
    }

    func applicationWillTerminate(application: UIApplication) {
    }


}

